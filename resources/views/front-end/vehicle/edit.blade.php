@extends('master_layout.layout')
@section('content')


    <div class="container"><br>

        <div class="col-md-8 offset-md-2">

            <div class="card">

                <h5 class="text-center card-header">Vehicle Update from</h5>

                <div class="card-body">

                    <div class="col-sm-12">
                        <form action="/vehicle/{{$edit->id}}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{method_field('PUT')}}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" value="{{$edit->name}}" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="name">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" <?php if($edit->bookable=='yes'){echo 'checked';} ?> name="bookable" type="checkbox" value="yes" id="defaultCheck1" >
                                    <label class="form-check-label" for="defaultCheck1">
                                        Bookable
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <img src="{{asset('upload/pictures/'.$edit->picture)}}" width="200px" height="100px"> <br/>
                            </div>

                            <div class="form-group">
                                <input type="file" name="picture" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Registation Number</label>
                                <input type="text" name="registration_number" value="{{$edit->registration_number}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="registration">
                            </div>
                            <div class="form-group">
                                <label for="notes">Notes</label>
                                <textarea cols="5" rows="5" name="notes" class="form-control" id="notes" aria-describedby="emailHelp" placeholder="notes">{{$edit->notes}}</textarea>

                            </div>
                            <button type="submit" class="btn btn-success">Update</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection



